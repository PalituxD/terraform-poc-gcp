
# terraform-poc-gcp


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Overview

This project creates an infrastruture that contains:
- 1 bucket: This input bucket executes a cloud function when a file is either uploaded or removed or renamed.
- 1 Cloud function: This function is called by the input bucket event.

## Pre-requisites
- terraform 1.1.5 and above (https://www.terraform.io/downloads)
- gcloud cli (https://cloud.google.com/sdk/docs/install)
- 1 GCP project with enabled APIs below.
    - Cloud Build API
    - Cloud Functions API
- 1 enabled key for connecting to the GCP project
- 1 bucket for terraform backend

## Installation
### Local
- Set environment variables:
    - CI_ENVIRONMENT_NAME=local|dev|prd
    - GOOGLE_APPLICATION_CREDENTIALS={{GCP key json file path of your GCP}}  (used for creating GCP resources in the desired GCP accoutnt/environment) 
    - GOOGLE_CREDENTIALS={{GCP key json file content}} (used for terraform backend only, could be shared for all environment)
    - TERRAFORM_BACKEND_BUCKET={{backend bucket name}}
    - TF_VAR_project_id={{GCP project id}}

### Gitlab
- At gitlab -> Settings -> CI/CD, create variables below according to your environment:
    - GOOGLE_APPLICATION_CREDENTIALS={{GCP key json file path of your GCP}} as Variable (used for creating GCP resources in the desired GCP accoutnt/environment)
    - GOOGLE_CREDENTIALS={{GCP key json file content}} as FILE (used for terraform backend only, could be shared for all environment)
    - TERRAFORM_BACKEND_BUCKET={{backend bucket name}} as Variable
    - TF_VAR_project_id={{GCP project id}} as Variable

More information about:
- GOOGLE_APPLICATION_CREDENTIALS and GOOGLE_CREDENTIALS vars: https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started
- TF_VAR_ vars: https://www.terraform.io/cli/config/environment-variables#tf_var_name


## Test and Deploy
### Local
```
cd terraform
rm -rf .terraform
terraform --version
terraform init -backend-config "bucket=$TERRAFORM_BACKEND_BUCKET"
terraform workspace new $CI_ENVIRONMENT_NAME || terraform workspace select $CI_ENVIRONMENT_NAME
terraform plan -out terraform.plan
terraform apply terraform.plan
#### To detroy the infrastructure:
# terraform plan
# terraform destroy -auto-approve
```
### Gitlab
- When a branch is either created or updated, gitlab executes validate_plan job
- When the main branch is updated, gitlab:
    -  executes the validate_plan job.
    -  dev job is executed automatically 
    -  prd job is enabled for being executed manually. 


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.
