terraform {

  # USE GOOGLE_CREDENTIALS variable, JSON content from GCP key 
  # https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started
  backend "gcs" {
  }

  required_providers {
    google = {
      version = "~> 4.0.0"
    }
  }
}
