resource "google_storage_bucket" "project-bucket" {
  name          = "${var.project_id}-bucket-${terraform.workspace}"
  location      = var.region
  force_destroy = true
}

resource "google_storage_bucket" "input_bucket" {
    name     = "${var.project_id}-input-${terraform.workspace}"
    location = var.region
    force_destroy = true
}
