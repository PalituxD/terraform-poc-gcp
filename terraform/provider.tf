# USE GOOGLE_APPLICATION_CREDENTIALS per GCP ACCOUNT, path where GCP key is stored
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started
provider "google" {
  project = var.project_id
  region  = var.region
}
