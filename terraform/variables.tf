variable "project_id" {
    default = "terraform-poc-XXXXXXXXX"
    type = string
}

variable "region" {
    default = "us-east1"
    type = string
}
