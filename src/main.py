import os

def hello_gcs(event, context):
    print('Bucket:' + event['bucket'])
    print('File:' + event['name'])
    print('Message: Hello World')
    print('Environment:'  + os.environ.get('ENVIRONMENT', 'Specified environment variable is not set.'))
